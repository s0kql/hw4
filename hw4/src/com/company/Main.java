package com.company;

import com.company.figures.Figures;
import com.company.figures.Rectangle;
import com.company.figures.Square;
import com.company.figures.Triangle;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите сторону (высоту) : ");
        int Side1 = in.nextInt();
        System.out.print("Введите сторону (основание) : ");
        int Side2 = in.nextInt();

        Rectangle rectangle = new Rectangle(Side1, Side2);
        System.out.print("Площадь прямоугольника = ");
        rectangle.getArea();

        Square square = new Square(Side1, Side2);
        System.out.print("Площадь квадрата = ");
        square.getArea();

        Triangle triangle = new Triangle(Side1, Side2);
        System.out.print("Площадь прямоугольного треугольника = ");
        triangle.getArea();

    }
}
