package com.company.figures;

public class Rectangle extends Figures  {
    public Rectangle(int Side1, int Side2) {
        super(Side1, Side2);
    }

    public void getArea() {
        double area = this.getSide1() * this.getSide2();
        System.out.println(area);
    }


}
