package com.company.figures;

public class Triangle extends Figures {
    public Triangle(int Side1, int Side2) {
        super(Side1, Side2);
    }

    public void getArea() {
        double area = this.getSide1() * this.getSide2() / 2.0d;
        System.out.println(area);


    }
}
