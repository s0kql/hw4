package com.company.figures;

public abstract class figures {
    private int side1;
    private int side2;

    public figures(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public double getside1() {
        return (double)this.Side1;
    }

    public double getside2() {
        return (double)this.side2;
    }

    public abstract void getarea();


}
